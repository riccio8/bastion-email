﻿namespace Bastion.EMail.SendGridSender.Settings
{
    /// <summary>
    /// SendGrid settings.
    /// </summary>
    public class SendGridSettings
    {
        /// <summary>
        /// Gets or sets key.
        /// </summary>
        public string Key { get; set; }
    }
}
