﻿namespace Bastion.EMail.SendGridSender
{
    using Bastion.EMail.Core;
    using Bastion.EMail.Core.Settings;
    using Bastion.EMail.SendGridSender.Settings;

    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    using SendGrid;
    using SendGrid.Helpers.Mail;

    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// Email Sender.
    /// </summary>
    public class SendGridEMailSender : EMailSenderBase
    {
        private readonly SendGridClient clientSendGrid;

        /// <summary>
        /// Initializes a new instance of <see cref="SendGridEMailSender"/>.
        /// </summary>
        public SendGridEMailSender(
            ILogger<SendGridEMailSender> logger,
            IOptions<EMailSenderSettings> options,
            IOptions<SendGridSettings> optionsSendGrid)
            : base(logger, options)
        {
            try
            {
                clientSendGrid = new SendGridClient(optionsSendGrid.Value.Key);
            }
            catch (Exception ex)
            {
                logger.LogError($"SendGrid, initialize error: {ex.Message}");
            }
        }

        /// <summary>
        /// Send Email.
        /// </summary>
        /// <param name="email">EMail.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="htmlMessage">Message.</param>
        public override Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            if (settings.Enabled)
            {
                var msg = new SendGridMessage
                {
                    Subject = subject,
                    HtmlContent = htmlMessage,
                    From = new EmailAddress(settings.FromEMail, settings.Company),
                };
                msg.AddTo(new EmailAddress(email));
                msg.SetClickTracking(false, false);

                logger.LogInformation($"SendGrid, send email to: {email}, subject: {subject}");

                return clientSendGrid?.SendEmailAsync(msg);
            }
            return Task.CompletedTask;
        }
    }
}
