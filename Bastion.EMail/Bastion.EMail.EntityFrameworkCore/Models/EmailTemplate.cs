﻿namespace Bastion.EMail.EntityFrameworkCore.Models
{
    using System.Collections.Generic;

    public partial class EMailTemplate
    {
        public string TemplateId { get; set; }
        public string PreView { get; set; }
        public string Subject { get; set; }
        public string Html { get; set; }

        public virtual ICollection<ChangeHistory> ChangeHistories { get; set; } = new HashSet<ChangeHistory>();
    }
}
