﻿namespace Bastion.EMail.EntityFrameworkCore.Models
{
    using System;

    public partial class ChangeHistory
    {
        public string TemplateId { get; set; }
        public string PreView { get; set; }
        public string Subject { get; set; }
        public string Html { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UserId { get; set; }

        public virtual EMailTemplate Template { get; set; }
    }
}
