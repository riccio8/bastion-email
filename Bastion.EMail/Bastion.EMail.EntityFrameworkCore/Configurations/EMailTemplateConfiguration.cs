﻿namespace Bastion.EMail.EntityFrameworkCore.Configurations
{
    using Bastion.EMail.EntityFrameworkCore.Models;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal class EMailTemplateConfiguration : IEntityTypeConfiguration<EMailTemplate>
    {
        public void Configure(EntityTypeBuilder<EMailTemplate> builder)
        {
            builder.ToTable("email_template", "public");

            builder.HasKey(e => e.TemplateId)
                .HasName("email_html_pkey");

            builder.Property(e => e.TemplateId)
                .HasColumnName("template_id")
                .HasMaxLength(50)
                .ValueGeneratedNever();

            builder.Property(e => e.Html)
                .IsRequired()
                .HasColumnName("html");

            builder.Property(e => e.PreView)
                .HasColumnName("preView");

            builder.Property(e => e.Subject)
                .HasColumnName("subject");
        }
    }
}
