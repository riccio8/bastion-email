﻿namespace Bastion.EMail.EntityFrameworkCore.Configurations
{
    using Bastion.EMail.EntityFrameworkCore.Models;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal class ChangeHistoryConfiguration : IEntityTypeConfiguration<ChangeHistory>
    {
        public void Configure(EntityTypeBuilder<ChangeHistory> builder)
        {
            builder.ToTable("email_template_change_history", "public");

            builder.HasKey(e => new { e.TemplateId, e.CreatedAt })
                .HasName("email_template_change_history_pkey");

            builder.Property(e => e.UserId)
                .IsRequired()
                .HasColumnName("changed_by_user_id")
                .HasMaxLength(36);

            builder.Property(e => e.Html)
                .IsRequired()
                .HasColumnName("html");

            builder.Property(e => e.PreView)
                .HasColumnName("preView");

            builder.Property(e => e.Subject)
                .HasColumnName("subject");

            builder.Property(e => e.TemplateId)
                .IsRequired()
                .HasColumnName("template_id")
                .HasMaxLength(50);

            builder.Property(e => e.CreatedAt)
                .HasColumnName("ts")
                .HasDefaultValueSql("now()");

            builder.HasOne(d => d.Template)
                .WithMany(p => p.ChangeHistories)
                .HasForeignKey(d => d.TemplateId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("email_template_change_history_template_id_fkey");
        }
    }
}
