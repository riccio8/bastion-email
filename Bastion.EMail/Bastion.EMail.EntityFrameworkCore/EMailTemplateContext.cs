﻿namespace Bastion.EMail.EntityFrameworkCore
{
    using Bastion.EMail.EntityFrameworkCore.Configurations;
    using Bastion.EMail.EntityFrameworkCore.Models;

    using Microsoft.EntityFrameworkCore;

    public partial class EMailTemplateContext : DbContext
    {
        public EMailTemplateContext()
        {
        }

        public EMailTemplateContext(DbContextOptions<EMailTemplateContext> options)
            : base(options)
        {
        }

        public virtual DbSet<EMailTemplate> EMailTemplates { get; set; }
        public virtual DbSet<ChangeHistory> ChangeHistories { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new EMailTemplateConfiguration());
            modelBuilder.ApplyConfiguration(new ChangeHistoryConfiguration());
        }
    }
}
