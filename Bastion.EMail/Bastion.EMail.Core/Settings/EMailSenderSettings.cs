﻿namespace Bastion.EMail.Core.Settings
{
    /// <summary>
    /// EMailSender settings.
    /// </summary>
    public class EMailSenderSettings
    {
        /// <summary>
        /// Gets or sets Enabled.
        /// </summary>
        public bool Enabled { get; set; } = false;

        /// <summary>
        /// Gets or sets EMail from.
        /// </summary>
        public string FromEMail { get; set; }

        /// <summary>
        /// Gets or sets Company.
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Gets or sets Images url.
        /// </summary>
        public string ImagesUrl { get; set; }

        /// <summary>
        /// Gets or sets Link help.
        /// </summary>
        public string LinkHelp { get; set; }

        /// <summary>
        /// Gets or sets Link welcome.
        /// </summary>
        public string LinkWelcome { get; set; }

        /// <summary>
        /// Gets or sets Link support email.
        /// </summary>
        public string LinkSupportEmail { get; set; }

        /// <summary>
        /// Gets or sets Link forgot password.
        /// </summary>
        public string LinkForgotPassword { get; set; }

        /// <summary>
        /// Gets or sets Link investor deposit.
        /// </summary>
        public string LinkInvestorDeposit { get; set; }

        /// <summary>
        /// Gets or sets Link email confirmation.
        /// </summary>
        public string LinkEmailConfirmation { get; set; }

        /// <summary>
        /// Gets or sets Link email confirmation.
        /// </summary>
        public string LinkDeviceConfirmation { get; set; }
    }
}
