﻿namespace Bastion.EMail.Core
{
    using Bastion.EMail.Core.Settings;

    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// Email Sender base.
    /// </summary>
    public abstract class EMailSenderBase
    {
        protected readonly EMailSenderSettings settings;
        protected readonly ILogger<EMailSenderBase> logger;

        /// <summary>
        /// Initializes a new instance of <see cref="EMailSenderBase"/>.
        /// </summary>
        public EMailSenderBase(
            ILogger<EMailSenderBase> logger,
            IOptions<EMailSenderSettings> options)
        {
            this.logger = logger;
            settings = options.Value;
        }

        /// <summary>
        /// Send Email.
        /// </summary>
        /// <param name="email">EMail.</param>
        /// <param name="subject">Subject.</param>
        /// <param name="htmlMessage">Message.</param>
        public abstract Task SendEmailAsync(string email, string subject, string htmlMessage);

        public Task SendNewDeviceEmail(string email, string firstName, string callBackUrlParameters, object p)
        {
            throw new NotImplementedException();
        }

        public Task SendEmailConfirmation(string email, string firstName, string callBackUrlParameters)
        {
            throw new NotImplementedException();
        }

        public Task SendPasswordRequestEmail(string email, string firstName, string callBackUrlParameters)
        {
            throw new NotImplementedException();
        }

        public Task SendPasswordResetSuccessEmail(string email, string firstName)
        {
            throw new NotImplementedException();
        }
    }
}
